

/**

*/

#ifndef LCTP_H
#define LCTP_H

#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <functional>
#include <ctime>
#include <mutex>
#include <chrono>

#define DEFAULT_LIMIT 10
#define VERBOSE_THREAD_POOL true
#define THRESHOLD 10

class LCTP {
    struct TimeRecord {
        struct TimeObject {
            double elapsedTime;
            time_t timestamp;
        };
        TimeRecord();
        void addTime(double newTime);
        double avg() const;
        TimeObject times[10];
        int index;
    };
 public:
    LCTP(std::vector<std::function<void()>>& vec, int limit=DEFAULT_LIMIT);

    /**
       Adds task to tasks vector.

       std::function<void()> is equivilent to a void lambda with no arguments. 
     */
    void addTask(std::function<void()>& task);

    /**
       Function for monitoring the task thread vector timings. 
       This is where the activeThreads 
     */
    void monitor();

    /**
       This should only be called ONCE. This joins each of the threads
       running in the task vectors and the monitor threads. Any task running
       in the tasks vector must finish before they can be joined. 
     */
    void stop();
 private:
    std::vector<std::function<void()>> tasks;
    std::vector<std::thread> threads;
    std::thread monitorThread;
    std::mutex threadIndexMutex;
    int limit, activeThreads, index=0;
    bool active=true, stopped=false;
    TimeRecord timeRecord;
};

#endif
