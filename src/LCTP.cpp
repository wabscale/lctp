#include "LCTP.h"

using namespace std;
//using namespace std::chrono_literals;

LCTP::TimeRecord::TimeRecord(): index(0) {
    for (int i=0; i<10; ++i) {
        times[i] = TimeObject{0, time(nullptr)};
    }
}
void LCTP::TimeRecord::addTime(double newTime) {
    times[index] = TimeObject{newTime, time(nullptr)};
    index = (index + 1) % 10;
}

double LCTP::TimeRecord::avg() const {
    double sum=0.;
    int count=0;
    for (int i=0; i<10; ++i)
        if (times[i].elapsedTime != 0. &&
            time(nullptr) - times[i].timestamp < 10) {
            sum += times[i].elapsedTime;
            ++count;
        }
    if (count == 0)
        return 0.;
    return sum / count;
}


LCTP::LCTP(vector<function<void()>>& vec, int limit):
    tasks(vec), limit(limit), activeThreads(limit) {
    monitorThread = thread([&](){ this->monitor(); });
    auto threadLoop = [&] (int threadNumber) {
        while (this->active) {
            if (threadNumber <= activeThreads) {
                threadIndexMutex.lock();
                int i = index;
                index = (index + 1) % 10;
                threadIndexMutex.unlock();
                
                time_t start(time(nullptr));
                tasks[i]();
                double elapsed = double(time(nullptr) - double(start));
                timeRecord.addTime(elapsed);
            } else {
                this_thread::sleep_for(chrono::seconds(1));
            }
        }
    };
    for (int threadNum=0; threadNum<limit; ++threadNum) {
        threads.push_back(thread(threadLoop, threadNum));
    }
}

void LCTP::addTask(function<void()>& task) {
    tasks.push_back(task);
}

void LCTP::monitor() {
    auto pdiff = [](double a, double b) {
        return ((a - b) / a) * 100.;
    };
    int index = 0;
    string spinner("\\|/-");
    while (this->active) {
        this_thread::sleep_for(chrono::seconds(1));
        double avgTime(timeRecord.avg());
        double pTimeDiff = pdiff(avgTime, (60. * activeThreads) / limit);
        
        if (pTimeDiff > THRESHOLD / 2. && activeThreads < limit) {
            ++activeThreads;
        } else if (pTimeDiff < -THRESHOLD / 2. && activeThreads > 1) {
            --activeThreads;
        }
        //time_t t = time(nullptr);
        if (VERBOSE_THREAD_POOL && active) {
            cout << pTimeDiff <<  "% "
                 << "active threads: " << activeThreads
                 << ' ' << spinner[index] << "      \r";
            index = (index + 1) % spinner.size();
            cout.flush();
        }
    }
}

void LCTP::stop(){
    if (stopped) {
        cerr << "Thread Pool already stopped\n";
        exit(1);
    }
    if (VERBOSE_THREAD_POOL) 
        cout << "\nstop\n";
    active = false;
    monitorThread.join();
    for (thread& t: threads)
        t.join();
    stopped=true;

}
