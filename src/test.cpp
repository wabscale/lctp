#include <iostream>
#include <string>
#include "LCTP.h"


using namespace std;

int main(int argc, char* argv[]) {
    vector<function<void()> > vec;
    for (int i=0; i<100; ++i) {
        vec.push_back([]() {
            this_thread::sleep_for(chrono::milliseconds(6000));
        });
    }
    LCTP threadPool(vec);

    char charInput;
    while (cin >> charInput) {
        if (charInput == 'q') {
            threadPool.stop();
            break;
        }
    }
}
