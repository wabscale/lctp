
TEST_BIN_NAME=test
OBJ_NAME=LCTP
OPTIONS=-Wall --std=c++11 -I./include/
LIBS=-pthread

compile:
	g++ $(OPTIONS) -o bin/$(TEST_BIN_NAME) src/*.cpp $(LIBS)

run: compile
	./bin/$(TEST_BIN_NAME)
